# Troubleshooting

Tips on how to deal with some common errors.

## Control characters are not allowed

**Problem**: This error can occur if you try to run `./tutor NAME k8s quickstart` before terraform has finished applying your changes and creating your cluster.

    :::text
    error: error loading config file "/root/.kube/config": yaml: control characters are not allowed
    Error: Command failed with status 1: kubectl apply --kustomize /workspace/env --wait --selector app.kubernetes.io/component=namespace

**Solution**: Let the wrapper script regenerate the config file

    :::bash
    cd my-cluster/control/
    rm ../kubeconfig-private.yml

## Provisioning the first Open edX instance fails

**Problem**: This error can occur when you run `/tutor NAME config save --interactive` when provisioning the first Open edX instance. The execution of `get_kubeconfig_path` will error out with an error related to filesystem permissions.

**Solution**: Bypass the calls to `get_kubeconfig_path` and write the output manually using:

    :::bash
    cd my-cluster/control/
    ./tf output -raw kubeconfig > ../kubeconfig-private.yml

## Namespace "monitoring" not found

**Problem**: This error can occur if the initial creation of resources took to long to provision.
**Solution**: Run `./tf plan` and `./tf apply` again.

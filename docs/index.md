# Grove

Grove is a set of GitLab CI pipelines, bootstrapping scripts, and wrappers that make it easy to deploy Open edX via [Tutor](https://docs.tutor.overhang.io/index.html) onto AWS and DigitalOcean backed Kubernetes clusters. Grove is not an official part of Tutor nor Open edX; it is developed for the community by [OpenCraft](https://opencraft.com/).

By using Grove, you get [Open edX](https://open.edx.org/) instances deployed to a [Kubernetes](https://kubernetes.io/) cluster by [Tutor](https://docs.tutor.overhang.io/index.html) and [GitLab CI](https://docs.gitlab.com/ee/ci/), on infrastructure provisioned by [Terraform](https://www.terraform.io/).

## Goals

The goal of the Grove project is to

- Provide an easy way to deploy Open edX in Kubernetes via Tutor.
- Support multiple Open edX in a single Kubernetes cluster.
- Make it easy to customize Open edX configurations, themes, etc.
- Make it easy to mass upgrade all instances in a Kubernetes cluster.
- Support multiple providers like AWS, DigitalOcean or minikube for local development.

## Project status

The project is under active development. Grove fully supports AWS, DigitalOcean and minikube as providers.

## Project roadmap

Feature requests, bugs, and improvement ideas are managed on the project's [issue tracker](https://gitlab.com/opencraft/dev/grove/-/issues?sort=created_date&state=opened), available for everyone in the community. To have a better visibility in the order of implementation and dependencies, please take a look at the [roadmap](https://view.monday.com/2544359996-079b70c47807e84a95715e272b2f9073?r=use1).

# New Relic Monitoring

It's possible to monitor instances for uptime via New Relic.

Add the following variables to `Settings > CI / CD > Variables` in Gitlab or to `private.yml` if you are working locally:

- `NEW_RELIC_API_KEY` - Your new relic Admin API key for the Rest v2 API.
- `NEW_RELIC_MONITORING_EMAILS` - A list of emails of that will get the alerts.
- `NEW_RELIC_REGION_CODE` - **us** or **eu** depending on your New Relic account's region.

Once an instance is deployed Synthetic monitors will be set up for:

- `LMS_HOST`
- `CMS_HOST`
- `PREVIEW_LMS_HOST`
- `LMS_HOST/heartbeat`

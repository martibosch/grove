# Grove

[![Pipeline Status](https://gitlab.com/opencraft/dev/grove/badges/main/pipeline.svg)](https://gitlab.com/opencraft/dev/grove/-/commits/main)

Grove is a set of GitLab CI pipelines, bootstrapping scripts, and wrappers that make it easy to deploy Open edX via [Tutor](https://docs.tutor.overhang.io/index.html) onto AWS, DigitalOcean and minikube backed Kubernetes clusters. Grove is not an official part of Tutor nor Open edX; it is developed for the community by [OpenCraft](https://opencraft.com/).

Check out the documentation at [https://grove.opencraft.com](https://grove.opencraft.com) to learn more about Grove.

## Why should you use Grove

By using Grove, you get [Open edX](https://open.edx.org/) instances deployed to a [Kubernetes](https://kubernetes.io/) cluster by [Tutor](https://docs.tutor.overhang.io/index.html) and [GitLab CI](https://docs.gitlab.com/ee/ci/), on infrastructure provisioned by [Terraform](https://www.terraform.io/).

Grove keeps everything in hand and manages the floating pieces for you. You don't need to set up a Kubernetes cluster manually nor reinvent the wheel by figuring out every bit of the CI/CD pipelines, Tutor configurations or configuring the OpenEdX instances.

This project is open-source and free to use, ensuring the sustainability of the approach.

## Getting started

To start using Grove, please check the [Getting started](https://grove.opencraft.com/getting-started/) documentation.

## Shared resources

Grove uses shared resources for persistent and/or pricy infrastructure components, like MySQL and MongoDB. Every instance has its own credentials to access the resources and operate only on the dedicated database resources. Instances cannot operate on each other's resources (eg. database tables).

## Contributing

This repository was split into two pieces:

* [Grove](https://gitlab.com/opencraft/dev/grove/-/tree/main) -- the core component, and
* [Grove template](https://gitlab.com/opencraft/dev/grove-template/-/tree/main) -- which contains cluster and deployment-specific configuration

Grove template utilizes Grove as a git submodule. This way, users of Grove can store the cluster configuration privately, while the core can be updated separately.

## Project status

Grove The project is under active development, but fully supports both AWS and DigitalOcean providers.

## Project roadmap

Feature requests, bugs, and improvement ideas are managed on the project's [issue tracker](https://gitlab.com/opencraft/dev/grove/-/issues?sort=created_date&state=opened), available to everyone in the community. To have a better visibility on the order of implementation and dependencies, please take a look at the [roadmap](https://view.monday.com/2544359996-079b70c47807e84a95715e272b2f9073?r=use1).

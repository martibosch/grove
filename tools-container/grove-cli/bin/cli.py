#!/usr/bin/python3
"""
Grove CLI commands are placed here.
"""
import json
from typing import List, Optional

import typer
import yaml

from grove.exceptions import GroveError
from grove.handlers import base_image_upgrade_handler
from grove.instance import Instance
from grove.pipeline import abort_pipeline, commit_based_pipeline
from grove.utils import instance_customization_from_trigger_payload

app = typer.Typer()
tutor_app = typer.Typer()
app.add_typer(
    tutor_app,
    name="tutor",
    help="Tutor wrapper commands, run `grove tutor --help` for more.",
)


@app.command()
def new(instance_name: str):
    """
    Create a new Open edX instance

    This command will copy the default configuration and create a new instance
    $INSTANCE_NAME in the instances/$INSTANCE_NAME directory. This command
    should not have any dependency on Terraform. But it uses Tutor to prepare
    the initial configuration.
    """
    typer.echo(f"Creating new instance: {instance_name}")
    try:
        instance = Instance(instance_name)
        instance.create()
        typer.echo("Done!")
    except GroveError as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def prepare(instance_name: str):
    """
    Prepares a new Open edX instance, creates if not exists. Primarily used by CI.
    """
    try:
        instance = Instance(instance_name)
        instance.prepare(instance_customization_from_trigger_payload())
        typer.echo(f"Instance {instance_name} prepared!")
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def pipeline(
    commit_text: str,
    output_file: str,
):
    """
    Generate Pipelines based on commit text.

    Given a `commit_text`, Grove CLI will generate the pipeline for it and write
    it on `output_file` path. This command will be used by GitLab CI to
    generate child pipeline based on commit messages.
    Check commit based pipeline[0] for more info.

    [0] - https://grove.opencraft.com/discoveries/0002-commit-based-deployment-pipeline
    """
    pipeline = commit_based_pipeline(commit_text)
    with open(output_file, "w") as file:
        yaml.safe_dump(pipeline, file)


@app.command()
def webhookpipeline(
    payload_file: str = typer.Argument(None, envvar="TRIGGER_PAYLOAD"),
    output: str = typer.Option(None, help="Output File Path for pipeline"),
):
    """
    Create pipelines from GitLab and GitHub webhook payload from $TRIGGER_PAYLOAD.
    Only to be used from CI.
    """
    if payload_file:
        with open(payload_file) as file:
            payload = json.load(file)
        pipeline = base_image_upgrade_handler(payload)
        if output:
            with open(output, "w") as file:
                yaml.safe_dump(pipeline, file)
        else:
            typer.echo("No file provided to write pipeline.")
            typer.echo(pipeline)
    else:
        typer.echo("payload file is required!")
        raise typer.Abort()


@app.command()
def abortpipeline(pipeline_id: int = typer.Argument(None, envvar="PIPELINE_ID")):
    """
    Cancels all the deployment jobs of the specified pipeline when called from
    the CI using the $ABORT_DEPLOYMENT_TRIGGER and the $PIPELINE_ID.

    Or when called from local using `./grove abortpipeline <pipeline_id>`
    """
    if pipeline_id:
        typer.echo(f"Stopping the pipeline {pipeline_id} and all its child pipelines")
        stopped = abort_pipeline(pipeline_id)
        typer.secho(f"{stopped} pipelines were aborted", fg=typer.colors.GREEN)
        if not stopped:
            raise typer.Abort()
    else:
        typer.echo("Pipeline ID is required to abort deployment")
        raise typer.Abort()


@app.command()
def archive(instance_name: str):
    """
    Archive an instance by removing its namespace, but keep infrastructure components.
    """

    try:
        instance = Instance(instance_name)
        instance.archive()
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def predeploy(instance_name: str):
    """
    Perform steps to be done before image build and deployment.
    Ex - clone custom theme, override scss, etc.
    """
    try:
        instance = Instance(instance_name)
        instance.pre_deploy()
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def deploy(instance_name: str):
    """
    Deploy an Open edX instance
    """
    typer.echo(f"Deploying instance {instance_name}")
    try:
        instance = Instance(instance_name)
        instance.deploy()
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def postdeploy(instance_name: str):
    """
    Do post-deployment work, ex: enable the theme, update images, etc.
    """
    typer.echo(f"Deploying instance {instance_name}")
    try:
        instance = Instance(instance_name)
        instance.post_deploy()
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@tutor_app.command()
# pylint: disable=redefined-builtin
def exec(
    instance_name: str,
    command_str: str,
    format: str = typer.Option("string", help="Format of command_str. JSON supported."),
):
    """
    Runs any tutor command.

    Supports passing command_str as JSON string. This helps escaping different characters
    while running from Grove ruby wrapper.
    """
    instance = Instance(instance_name)
    if format == "json":
        command_str = json.loads(command_str)
    instance.tutor_env_pull()
    instance.tutor_save_config()
    instance.tutor_env_override()
    instance.tutor_command(command_str)
    instance.tutor_env_push()


@tutor_app.command()
def sync(instance_name: str):
    """
    Build and sync tutor env directory with s3
    """
    instance = Instance(instance_name)
    instance.tutor_env_sync()


@tutor_app.command()
def buildimage(
    instance_name: str,
    image_name: str,
    edx_platform_repository: Optional[str] = None,
    edx_platform_version: Optional[str] = None,
    cache_from: Optional[List[str]] = typer.Option([], help="Use cache from provided images."),
):
    """
    Build an image using Tutor.
    """
    instance = Instance(instance_name)
    instance.tutor_env_pull()

    tutor_cache_args = []
    for cache_image in cache_from:
        tutor_cache_args.append(f'--docker-arg="--cache-from" --docker-arg="{cache_image}"')

    build_args = ["--build-arg BUILDKIT_INLINE_CACHE=1"]
    if edx_platform_repository:
        build_args.append(f'--build-arg EDX_PLATFORM_REPOSITORY="{edx_platform_repository}"')

    if edx_platform_version:
        build_args.append(f'--build-arg EDX_PLATFORM_VERSION="{edx_platform_version}"')

    tutor_cache_arg = " ".join(tutor_cache_args)
    platform_build_args = " ".join(build_args)
    tutor_command = f"images build {image_name} {tutor_cache_arg} {platform_build_args}"
    instance.tutor_command(tutor_command)


if __name__ == "__main__":
    app(prog_name="grove")

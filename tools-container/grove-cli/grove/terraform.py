import json
import os
from functools import lru_cache
from urllib.parse import parse_qs, urlparse

import typer

from grove.const import SCRIPTS_DIR
from grove.exceptions import GroveError
from grove.type_defs import TutorEnv
from grove.utils import execute

TF_SCRIPT = str(SCRIPTS_DIR / "tf.sh")
TF_ALIAS = f"bash {TF_SCRIPT}"


@lru_cache
def get_terraform_state(key: str) -> dict:
    try:
        result = execute(f"{TF_ALIAS} output -json {key}", hide_output=True, capture_output=True)
    except GroveError as e:
        typer.echo(f"Unable to get {key} state from terraform")
        typer.echo(e)
        return {}

    return json.loads(result)


def is_infrastructure_ready(instance_name: str) -> bool:
    """
    Determine if infrastructure ready for an instance.
    """
    try:
        provider = os.environ.get("TF_VAR_cluster_provider")
        mongodb_state = get_terraform_state("mongodb")
        return provider == "minikube" or mongodb_state and mongodb_state.get("instances", {}).get(instance_name, False)
    except Exception as e:
        typer.echo("Unable to get state from terraform")
        typer.echo(e)
        return False


def get_env(instance_name: str) -> TutorEnv:
    """
    Prepare env variables from terraform state for an instance.
    """
    env: TutorEnv = {}

    instance_prefix = instance_name.replace("-", "_")

    mysql_state = get_terraform_state("mysql")
    if mysql_state:
        env.update(
            {
                "TUTOR_MYSQL_HOST": mysql_state["host"],
                "TUTOR_MYSQL_PORT": str(mysql_state["port"]),
                "TUTOR_MYSQL_ROOT_USERNAME": mysql_state["root_username"],
                "TUTOR_MYSQL_ROOT_PASSWORD": mysql_state["root_password"],
                # set MySQL application user. Ensure that the username is not larger than 32 characters
                "TUTOR_OPENEDX_MYSQL_USERNAME": f"{instance_prefix}_openedx"[:32],
                # set MySQL databases names. Database names can be at most 64 character long
                "TUTOR_OPENEDX_MYSQL_DATABASE": f"{instance_prefix}_openedx"[:64],
            }
        )

    mongodb_state = get_terraform_state("mongodb")

    if mongodb_state:
        mongodb_instance = mongodb_state["instances"][instance_name]
        mongodb_url = urlparse(mongodb_state["connection_string"])

        mongodb_host = (
            f"{mongodb_url.scheme}://"
            f"{mongodb_instance['username']}:{mongodb_instance['password']}@{mongodb_url.hostname}"
        )

        mongodb_params = parse_qs(mongodb_url.query)

        env.update(
            {
                "TUTOR_MONGODB_HOST": mongodb_host,
                "TUTOR_MONGODB_DATABASE": mongodb_instance["database"],
                "TUTOR_MONGODB_USERNAME": mongodb_instance["username"],
                "TUTOR_MONGODB_PASSWORD": mongodb_instance["password"],
                "TUTOR_FORUM_MONGODB_DATABASE": mongodb_instance["forum_database"],
                "TUTOR_GROVE_MONGODB_REPLICA_SET": mongodb_params["replicaSet"][0],
            }
        )

    provider = os.environ.get("TF_VAR_cluster_provider")
    bucket_state = get_terraform_state("edxapp_bucket")

    if bucket_state and instance_name in bucket_state:
        instance = bucket_state[instance_name]

        if provider == "aws":
            access_key = instance["s3_user_access_key"]["id"]
            secret_access_key = instance["s3_user_access_key"]["secret"]
            bucket_host = instance["s3_bucket"]["bucket_domain_name"]
            bucket_name = instance["s3_bucket"]["id"]
            bucket_region = instance["s3_bucket"]["region"]
        elif provider == "digitalocean":
            access_key = os.environ.get("SPACES_ACCESS_KEY_ID")
            secret_access_key = os.environ.get("SPACES_SECRET_ACCESS_KEY")
            bucket_host = instance["spaces_bucket"]["bucket_domain_name"]
            bucket_name = instance["spaces_bucket"]["name"]
            bucket_region = instance["spaces_bucket"]["region"]

        env.update(
            {
                "TUTOR_OPENEDX_AWS_ACCESS_KEY": access_key,
                "TUTOR_OPENEDX_AWS_SECRET_ACCESS_KEY": secret_access_key,
                "TUTOR_S3_HOST": bucket_host,
                "TUTOR_S3_STORAGE_BUCKET": bucket_name,
                "TUTOR_S3_REGION": bucket_region,
            }
        )

    return env

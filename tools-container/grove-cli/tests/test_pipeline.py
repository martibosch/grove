from unittest.mock import patch

import pytest

from grove.instance import Instance
from grove.pipeline import (
    AbortPipelineExecutor,
    OpenedXPipeline,
    commit_based_pipeline,
    parse_instances,
)

from .utils import TEST_INSTANCE_NAME_A, TEST_INSTANCE_NAME_B, GroveTestCase


class TestPipelines(GroveTestCase):
    def test_parse_instances(self):

        instance_a = Instance(TEST_INSTANCE_NAME_A)
        instance_a.prepare({})

        instance_b = Instance(TEST_INSTANCE_NAME_B)
        instance_b.prepare({})

        instances = parse_instances("")
        assert len(instances) == 0

        instances = parse_instances(instance_a.instance_name)
        assert len(instances) == 1
        assert instances[0].instance_name == instance_a.instance_name

        instances = parse_instances(f"{instance_a.instance_name}|1")
        assert len(instances) == 1
        assert instances[0].instance_name == instance_a.instance_name

        instances = parse_instances(f"{instance_a.instance_name} |1, {instance_b.instance_name}|2")
        assert len(instances) == 2
        assert instances[0].instance_name == instance_a.instance_name
        assert instances[1].instance_name == instance_b.instance_name

        instance_a.delete()
        instance_b.delete()

    def test_openedx_pipeline(self):
        grove_config = {"IMAGE_BUILD_CONFIG": {"openedx": {"DOCKER_IMAGE_VAR": "DOCKER_IMAGE_OPENEDX"}}}
        tutor_config = {
            "EDX_PLATFORM_REPOSITORY": "https://github.com/edx/edx-platform",
            "EDX_PLATFORM_VERSION": "open-release/maple.1",
        }

        instance_a = Instance("a")
        instance_a._grove_config = grove_config
        instance_a._tutor_config = tutor_config
        instance_b = Instance("b")
        instance_b._grove_config = grove_config
        instance_b._tutor_config = tutor_config
        instances = [instance_a, instance_b]
        pipeline = OpenedXPipeline(instances).build()
        jobs_keys = pipeline.keys()

        assert "Prepare a" in jobs_keys
        assert "Prepare b" in jobs_keys

        assert "Build a - openedx" in jobs_keys
        assert "Build b - openedx" in jobs_keys

        assert "Deploy a" in jobs_keys
        assert "Deploy b" in jobs_keys

    def test_commit_based_pipeline_dummy(self):
        pipeline = commit_based_pipeline("Commit without any pipeline")
        assert "dummy" in pipeline.keys()

    def test_commit_based_pipeline_terraform(self):
        pipeline = commit_based_pipeline("[AutoDeploy][Infrastructure] deploying infrastructure")

        assert len(pipeline["stages"]) == 3
        assert "terraform_init" == pipeline["stages"][0]
        assert "terraform_plan" == pipeline["stages"][1]
        assert "terraform_apply" == pipeline["stages"][2]

        assert "cluster infra init" in pipeline.keys()
        assert "cluster infra plan" in pipeline.keys()
        assert "cluster infra apply" in pipeline.keys()

    def test_commit_based_pipeline_create(self):
        instance = Instance(TEST_INSTANCE_NAME_A)
        instance.prepare({})

        pipeline = commit_based_pipeline(f"[AutoDeploy][Create] {instance.instance_name}|1")

        assert len(pipeline["stages"]) == 6
        assert "terraform_init" == pipeline["stages"][0]
        assert "terraform_plan" == pipeline["stages"][1]
        assert "terraform_apply" == pipeline["stages"][2]

        assert "grove_prepare" == pipeline["stages"][3]
        assert "grove_build" == pipeline["stages"][4]
        assert "grove_deploy" == pipeline["stages"][5]

        assert "cluster infra init" in pipeline.keys()
        assert "cluster infra plan" in pipeline.keys()
        assert "cluster infra apply" in pipeline.keys()

    @patch("grove.pipeline.filter_archived_instances")
    @patch("grove.pipeline.os")
    def test_commit_based_pipeline_archive(self, mock_os, mock_filter):
        instance = Instance(TEST_INSTANCE_NAME_A)
        instance.prepare({})

        mock_os.getenv.return_value = False
        mock_filter.return_value = [instance]

        pipeline = commit_based_pipeline(f"[AutoDeploy][Archive] {instance.instance_name}")

        mock_os.getenv.assert_called_once_with("ARCHIVE_INSTANCE_TRIGGER")
        assert len(pipeline["stages"]) == 1
        assert "grove_archive" == pipeline["stages"][0]

        jobs_keys = pipeline.keys()
        assert f"Archive {instance.instance_name}" in jobs_keys

    def test_commit_based_pipeline_archive_trigger(self):
        instance = Instance(TEST_INSTANCE_NAME_A)
        instance.prepare({"GROVE_ARCHIVED": False})

        pipeline = commit_based_pipeline(f"[AutoDeploy][Archive] {instance.instance_name}")

        assert len(pipeline["stages"]) == 1
        assert "dummy" == pipeline["stages"][0]
        assert "dummy" in pipeline.keys()

    @patch("grove.pipeline.os")
    def test_commit_based_pipeline_delete(self, mock_os):
        mock_os.getenv.return_value = False

        pipeline = commit_based_pipeline("[AutoDeploy][Delete] instance-not-exists-anymore")

        mock_os.getenv.assert_called_once_with("DELETE_INSTANCE_TRIGGER")
        assert len(pipeline["stages"]) == 3
        assert "terraform_init" == pipeline["stages"][0]
        assert "terraform_plan" == pipeline["stages"][1]
        assert "terraform_apply" == pipeline["stages"][2]

        assert "cluster infra init" in pipeline.keys()
        assert "cluster infra plan" in pipeline.keys()
        assert "cluster infra apply" in pipeline.keys()

    def test_merge_commit_pipeline(self):

        commit_text = f"""Merge branch 'deployment/testinstance/1' into 'shimulch/bb-5265' [AutoDeploy][Create] {TEST_INSTANCE_NAME_A}|1 See merge request opencraft/dev/grove-development!68"""  # noqa: E501

        instance = Instance(TEST_INSTANCE_NAME_A)
        instance.prepare({})
        pipeline = commit_based_pipeline(commit_text)

        assert len(pipeline["stages"]) == 6
        assert "terraform_init" == pipeline["stages"][0]
        assert "terraform_plan" == pipeline["stages"][1]
        assert "terraform_apply" == pipeline["stages"][2]

        assert "grove_prepare" == pipeline["stages"][3]
        assert "grove_build" == pipeline["stages"][4]
        assert "grove_deploy" == pipeline["stages"][5]

        assert "cluster infra init" in pipeline.keys()
        assert "cluster infra plan" in pipeline.keys()
        assert "cluster infra apply" in pipeline.keys()

    def test_commit_based_pipeline_update(self):

        instance = Instance(TEST_INSTANCE_NAME_A)
        instance.prepare({})
        pipeline = commit_based_pipeline(f"[AutoDeploy][Update] {instance.instance_name}|1")

        assert len(pipeline["stages"]) == 3
        assert "grove_prepare" == pipeline["stages"][0]
        assert "grove_build" == pipeline["stages"][1]
        assert "grove_deploy" == pipeline["stages"][2]

        jobs_keys = pipeline.keys()
        assert f"Prepare {instance.instance_name}" in jobs_keys
        assert f"Build {instance.instance_name} - openedx" in jobs_keys
        assert f"Deploy {instance.instance_name}" in jobs_keys

    def test_commit_based_pipeline_batch_update(self):

        instance = Instance(TEST_INSTANCE_NAME_A)
        instance.prepare(
            {
                "tutor": {
                    "EDX_PLATFORM_REPOSITORY": "git@github.com:edx/edx-platform.git",
                    "EDX_PLATFORM_VERSION": "open-release/lilac.3",
                }
            }
        )
        pipeline = commit_based_pipeline(
            "[AutoDeploy][Batch][Upgrade] https://github.com/edx/edx-platform.git|open-release/lilac.3"
        )

        assert len(pipeline["stages"]) == 4
        assert "base_image_build" == pipeline["stages"][0]
        assert "grove_prepare" == pipeline["stages"][1]
        assert "grove_build" == pipeline["stages"][2]
        assert "grove_deploy" == pipeline["stages"][3]

        jobs_keys = pipeline.keys()
        assert f"Prepare {instance.instance_name}" in jobs_keys
        assert f"Build {instance.instance_name} - openedx" in jobs_keys
        assert f"Deploy {instance.instance_name}" in jobs_keys

    def test_commit_based_pipeline_image_build(self):

        pipeline = commit_based_pipeline(
            "[AutoDeploy][Build][Image] https://github.com/edx/edx-platform.git|open-release/lilac.3"
        )

        assert len(pipeline["stages"]) == 1
        assert "base_image_build" == pipeline["stages"][0]

        jobs_keys = pipeline.keys()
        assert "build open-release/lilac.3 from https://github.com/edx/edx-platform.git" in jobs_keys


###############################################################################
# Abort Pipeline Tests
###############################################################################


@patch("grove.pipeline.AbortPipelineExecutor.get_bridges")
def test_get_child_pipelines(bridges):
    bridges.return_value = []
    exe = AbortPipelineExecutor(324312)
    assert exe.get_child_pipelines() == []

    # extracts the pipeline ids correctly
    bridges.return_value = [
        {"downstream_pipeline": {"id": 1}},
        {"downstream_pipeline": {"id": 2}},
        {"downstream_pipeline": {"id": 3}},
        {"downstream_pipeline": {"id": 4}},
    ]
    assert exe.get_child_pipelines() == [1, 2, 3, 4]


@patch("requests.get")
def test_get_bridges(get, gitlab_pipeline_bridges):
    get.return_value.json.return_value = gitlab_pipeline_bridges
    get.return_value.status_code = 200

    # return the list of bridges from the API response
    exe = AbortPipelineExecutor(324312)
    bridges = exe.get_bridges()
    get.assert_called_once()
    assert isinstance(bridges, list)
    assert 1 == len(bridges)

    # return an empty list in case of API error
    get.reset_mock()
    get.return_value.status_code = 400
    get.return_value.reason = "Bad Request"
    get.return_value.content = ""
    bridges = exe.get_bridges()
    get.assert_called_once()
    assert [] == bridges
    assert 0 == len(bridges)


@patch("requests.post")
def test_cancel_pipeline(post):
    post.return_value.status_code = 200

    exe = AbortPipelineExecutor(324312)
    assert exe.cancel_pipeline(10)
    post.assert_called_once()

    post.reset_mock()
    post.return_value.status_code = 400
    assert not exe.cancel_pipeline(11)
    post.assert_called_once()


@patch("requests.post")
@patch("requests.get")
def test_process(get, post, gitlab_pipeline_bridges):
    get.return_value.json.return_value = gitlab_pipeline_bridges
    get.return_value.status_code = 200
    post.return_value.status_code = 200

    exe = AbortPipelineExecutor(324312)
    exe.project_id = ""
    exe.pipeline_id = ""
    exe.gitlab_token = ""

    # all 3 values are missing
    with pytest.raises(ValueError):
        exe.process()

    # pipeline_id and gitlab_token are missing
    exe.project_id = 24377526
    with pytest.raises(ValueError):
        exe.process()

    # gitlab_token is missing
    exe.pipeline_id = 324312
    with pytest.raises(ValueError):
        exe.process()

    # all 3 values are available
    exe.gitlab_token = "test-token"
    assert exe.process() == 2  # parent + 1 child id
    get.assert_called_once()
    # The sameple data has one child bridge - so 2 post requests
    post.assert_called()

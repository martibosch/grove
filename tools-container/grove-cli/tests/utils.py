from pathlib import Path
from unittest.case import TestCase
from unittest.mock import patch

from grove.const import INSTANCES_DIR
from grove.utils import slugify_instance_name

TEST_INSTANCE_NAME_A = slugify_instance_name("test instance a")
TEST_INSTANCE_NAME_B = slugify_instance_name("test instance b")

CLEANUP_DIRS = [
    INSTANCES_DIR / TEST_INSTANCE_NAME_A,
    INSTANCES_DIR / TEST_INSTANCE_NAME_B,
]

DUMMY_TERRAFORM_TUTOR_ENV = {
    "TUTOR_MYSQL_HOST": "mysql",
    "TUTOR_MYSQL_PORT": "3306",
    "TUTOR_MYSQL_ROOT_USERNAME": "root_user",
    "TUTOR_MYSQL_ROOT_PASSWORD": "root_password",
    "TUTOR_OPENEDX_MYSQL_USERNAME": "instance_user",
    "TUTOR_OPENEDX_MYSQL_DATABASE": "instance_database",
    "TUTOR_REDIS_HOST": "redis",
    "TUTOR_REDIS_PORT": "4444",
    "TUTOR_REDIS_USERNAME": "redis_user",
    "TUTOR_REDIS_PASSWORD": "redis_password",
    "TUTOR_GROVE_OVERRIDE_GLOBAL_PREFIX": "openedx_",
}


FIXTURES_DIR = Path(__file__).parent / "fixtures"


class GroveTestCase(TestCase):
    """
    Common TestCase class that mocks terraform calls.
    """

    def setUp(self):
        tf_patcher = patch("grove.instance.terraform")
        tutor_env_pull_patcher = patch("grove.instance.tutor_env_pull")
        tutor_env_push_patcher = patch("grove.instance.tutor_env_push")

        self.mock_tf = tf_patcher.start()
        self.mock_tf.is_infrastructure_ready.return_value = True
        self.mock_tf.get_env.return_value = DUMMY_TERRAFORM_TUTOR_ENV

        self.mock_tutor_env_pull = tutor_env_pull_patcher.start()
        self.mock_tutor_env_push = tutor_env_push_patcher.start()

        self.addCleanup(tf_patcher.stop)

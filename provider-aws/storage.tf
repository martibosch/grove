####################################################################################################
## An S3 bucket to store the Tutor "state" (env folders)
####################################################################################################

module "tutor_env" {
  source        = "./s3"
  bucket_prefix = "tenv-${var.cluster_name}"
  tags = merge(local.default_tags, {
    Name = "Tutor State Bucket for ${var.cluster_name}"
  })
}

output "tutor_env_bucket" {
  sensitive = false
  value     = module.tutor_env.s3_bucket.id
}

####################################################################################################
## Create the necessary S3 buckets
####################################################################################################

module "instance_edxapp_bucket" {
  for_each = toset(var.tutor_instances)

  source        = "./s3"
  bucket_prefix = join("-", ["edx", replace(var.cluster_name, "_", "-"), replace(each.key, "_", "-")])
  tags = merge(local.default_tags, {
    Name = "edX app bucket for instance ${each.key}"
  })
}


output "edxapp_bucket" {
  sensitive = true
  value     = { for instance in var.tutor_instances : instance => module.instance_edxapp_bucket[instance] }
}
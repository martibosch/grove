# Changelog

All notable changes to this project will be documented in this file.

## [0.3.0] - 2022-08-29

**Bug Fixes**

- New relic code works with urls containing multiple dashes ([918b3db](https://gitlab.com/opencraft/dev/grove/-/commit/918b3dbc4b30fc53ac8c4cfcd8e4e068925c17b4))
- Simplify alert manager configuration to only require receivers ([1622c69](https://gitlab.com/opencraft/dev/grove/-/commit/1622c69aa83ba766a8ed0f7c7957e882ca4d9657))
- Use nerdgraph for fetching new relic monitors ([693175c](https://gitlab.com/opencraft/dev/grove/-/commit/693175c067b8f8aa6320bcdd6d33df7b99d63d70))
- Use nerdgraph for fetching new relic monitors ([eb7df1e](https://gitlab.com/opencraft/dev/grove/-/commit/eb7df1e5c9ea7a584e5e805b45195ef7383b74a1))

**Documentation**

- Add disaster recovery doc ([04e8450](https://gitlab.com/opencraft/dev/grove/-/commit/04e845095d2e8ace1f255c6258946c65d3c4ef38))
- Add note about credentials ([8f7b5d1](https://gitlab.com/opencraft/dev/grove/-/commit/8f7b5d1628720087b842815353f6962af69e1cc5))
- Add note about credentials ([382e8a0](https://gitlab.com/opencraft/dev/grove/-/commit/382e8a0129c6f640484069ad774e10db5e4b17f1))

**Features**

- Copy theme files into MFE build dir ([889cd98](https://gitlab.com/opencraft/dev/grove/-/commit/889cd9874b504d873115ab2337c401e951e561ab))
- Separate archive and delete pipelines ([7178dbd](https://gitlab.com/opencraft/dev/grove/-/commit/7178dbd98d765e691bc70380cbc1b835d94b8383))
- Add maintenance pages to Grove ([092b909](https://gitlab.com/opencraft/dev/grove/-/commit/092b9095e305cbcea2dd01cfc1c7904d6f68fdb4))
- Add minikube provider ([123a8fd](https://gitlab.com/opencraft/dev/grove/-/commit/123a8fdaa7396e239db5a59391584f844027f581))

**Miscellaneous Tasks**

- Update tutor-contrib-grove ([164b1b1](https://gitlab.com/opencraft/dev/grove/-/commit/164b1b1478b5ce951dc560fa6a16022eafab5c66))
- Bump tools-container version to 0.1.28 ([0e79198](https://gitlab.com/opencraft/dev/grove/-/commit/0e79198fb29627bc77e907fe8c908274ebef6ee7))
- Remove sql_require_primary_key override. no longer needed ([a57d85e](https://gitlab.com/opencraft/dev/grove/-/commit/a57d85e9361dde6371765147fc789047f2d2af82))
- Bump tools-container version ([b8b1b9b](https://gitlab.com/opencraft/dev/grove/-/commit/b8b1b9b1c00fa0c4548b311c41f6c84786824fca))
- Update container versions ([10c02e4](https://gitlab.com/opencraft/dev/grove/-/commit/10c02e4311040dac1d7b8949e512449a3bc2751b))
- Update tutor dependenices ([fccaec1](https://gitlab.com/opencraft/dev/grove/-/commit/fccaec1c895cbb9ef582597f1a32ff923abc6c14))

**Refactor**

- Use k8s-override patch ([251d5aa](https://gitlab.com/opencraft/dev/grove/-/commit/251d5aa38dba27b32d9be8d438df7acb54a69770))

## [0.2.0] - 2022-06-30

**Bug Fixes**

- Allow digital ocean spaces to be destroyed ([c7b23b7](https://gitlab.com/opencraft/dev/grove/-/commit/c7b23b70a619a3f698720bda8c4b31728b13dbbb))
- Gitlab aws runner aws version was not compatible with 4.10.0 ([9d7d14a](https://gitlab.com/opencraft/dev/grove/-/commit/9d7d14ac9f2de2a130b52fb48a3f8d2e436fafd5))

**Documentation**

- Add instance lifecycles documentation ([15c97a4](https://gitlab.com/opencraft/dev/grove/-/commit/15c97a42886399570f7b29ce3a5f3fa0f7bcbf34))
- Update contributing guide ([71b894b](https://gitlab.com/opencraft/dev/grove/-/commit/71b894b1244eb5ba4d4eed4abdbbd3439dc345f2))

**Features**

- Allow LMS and CMS env setting customization ([87ef1b5](https://gitlab.com/opencraft/dev/grove/-/commit/87ef1b588db2af3236070fff59873680441b3b9f))
- Implement Spaces storage for edx-platform in DO ([d1c2c9e](https://gitlab.com/opencraft/dev/grove/-/commit/d1c2c9ef5244b8814d5b128dce150550d0b9909b))
- Add HPA for k8s resources ([45250eb](https://gitlab.com/opencraft/dev/grove/-/commit/45250eb91ead17de4d560ec5d498952e2e5dc752))
- Seetup support for static files and static pages ([a493554](https://gitlab.com/opencraft/dev/grove/-/commit/a4935543689e0f77ca2cc5991607515425aa8268))
- Attach pod logs to notifier zip ([173cbc9](https://gitlab.com/opencraft/dev/grove/-/commit/173cbc93804aa4276088003efaf0f6440308a74d))
- Upgrade to tutor 14 ([d84839c](https://gitlab.com/opencraft/dev/grove/-/commit/d84839c79a19a4265e7cdbf347843fca1ed6e5a0))
- Add HPA configuration ([03a81b7](https://gitlab.com/opencraft/dev/grove/-/commit/03a81b714998e3e8111a9346e76a7a00ac257988))

**Miscellaneous Tasks**

- Prometheus now monitors all namespaces ([1bb065c](https://gitlab.com/opencraft/dev/grove/-/commit/1bb065cd00784aaac1c8817699c82633da1cd8a3))

## [0.1.0] - 2022-05-30

**Bug Fixes**

- Refactor s3 sync ([ca59f68](https://gitlab.com/opencraft/dev/grove/-/commit/ca59f68bcd121001d35c3de04149c3e360252b17))
- CI YAML indent issue ([9c87278](https://gitlab.com/opencraft/dev/grove/-/commit/9c87278dd228cebf40863c85e269e223a9ee31b9))
- Error during terraform init ([d10cb7f](https://gitlab.com/opencraft/dev/grove/-/commit/d10cb7f20f9012401a778218da69feaaa977514e))
- Tutor sync failing issue due to hyphen in name ([b77765e](https://gitlab.com/opencraft/dev/grove/-/commit/b77765e91a7580c42d845742cd8a71456e25feb0))
- Use the correct module paths for k8s ([2d9f55a](https://gitlab.com/opencraft/dev/grove/-/commit/2d9f55afc51655b9bc8e3a02079e3c251b2c4890))
- Remove yaml reserved characters from MongoDB password ([3e1d399](https://gitlab.com/opencraft/dev/grove/-/commit/3e1d3995a1839876bb00b9753b4ab612e050d444))
- Alert manager config parsing and added monitoring docs ([99bdae0](https://gitlab.com/opencraft/dev/grove/-/commit/99bdae0ab566b4dbd629e2f1b8c28d85e972bff2))
- Ensure env variable parsed correctly ([4237556](https://gitlab.com/opencraft/dev/grove/-/commit/4237556e9c6a953c686c0403a958d56aa3a5ba25))
- Update the k8s resources to include metadata ([eb8360c](https://gitlab.com/opencraft/dev/grove/-/commit/eb8360c2ba0b1e7d50c909df57d173b6bd965c4d))
- Generate users for db resources ([ea47764](https://gitlab.com/opencraft/dev/grove/-/commit/ea47764fdce584658536a2f9eb816365963a63c2))
- Support MySQL 8 on Tutor ([b1f495a](https://gitlab.com/opencraft/dev/grove/-/commit/b1f495a3d30e90067112e1e7efffb5fb6e2c50d5))
- Resolve several issues within Grove ([da028b5](https://gitlab.com/opencraft/dev/grove/-/commit/da028b520ef696e364ac21fdcb83aa1197dbc89d))
- Mongodb network container being created before the cluster ([734b236](https://gitlab.com/opencraft/dev/grove/-/commit/734b236b37574892f3f33caf9e38f2db23df0ced))
- Use kubectl context ([95a759e](https://gitlab.com/opencraft/dev/grove/-/commit/95a759eceaf66537c84353194bf19467c77bba67))
- Create instance on directory missing ([476e469](https://gitlab.com/opencraft/dev/grove/-/commit/476e469088d98fe7e1bec764dbe7710dadd8a35c))
- Allow empty commits ([0c9886c](https://gitlab.com/opencraft/dev/grove/-/commit/0c9886c88a0fc8132e70aeb28bd37d7c89c4f4e7))

**Documentation**

- Batch redeployment discovery ([c36cbbd](https://gitlab.com/opencraft/dev/grove/-/commit/c36cbbdf01302fb7e4146977d13c63574f60d41d))
- Ensure the documentation refers to the correct directory ([b18785f](https://gitlab.com/opencraft/dev/grove/-/commit/b18785f2abfd5c5ba17da0bbe46d44963b823488))
- Fix getting started typo ([0cf263d](https://gitlab.com/opencraft/dev/grove/-/commit/0cf263d5abfbf4d956a775e1204cd445c98ef94f))
- Added documentation on configuring new relic ([651e7f3](https://gitlab.com/opencraft/dev/grove/-/commit/651e7f3bbb12f455deca3a457e7268f1ed6b298a))
- Documented MongoDB Atlas setup for AWS ([ef63b40](https://gitlab.com/opencraft/dev/grove/-/commit/ef63b405497bca7d89cd0f978f1e5e9099633eea))
- Periodic builds discovery ([fe02761](https://gitlab.com/opencraft/dev/grove/-/commit/fe027613f1d48cb1eee996c0aa7c29c5edc742d5))
- Extend, reword and reorganize documentation ([6176978](https://gitlab.com/opencraft/dev/grove/-/commit/617697841fe09125a14a6075994a9572a5e92f8c))
- Add scaling deployments discovery ([431639f](https://gitlab.com/opencraft/dev/grove/-/commit/431639f16425f368a7f99c09ed46d77b01e44e07))
- Add documentation for commit-based pipelines ([5307ae3](https://gitlab.com/opencraft/dev/grove/-/commit/5307ae3bfe7f0219081af7415a16fb9a0e73f7ac))
- Add connecting to instances ([f20ab30](https://gitlab.com/opencraft/dev/grove/-/commit/f20ab303c2721817b9998e621843a2bed46bd5cc))
- Add contributing docs ([b6cf94b](https://gitlab.com/opencraft/dev/grove/-/commit/b6cf94bb6be54a1b3dbb11681faa35e3ce85a639))
- Add releases discovery ([9c75a47](https://gitlab.com/opencraft/dev/grove/-/commit/9c75a475d63cf1f46894ee4a65ac1aaf412707a9))
- Added documentation on the monitoring ingress ([c3e072a](https://gitlab.com/opencraft/dev/grove/-/commit/c3e072aafab5b8528b685e42109d273742ef4d9b))
- Add maintenance pages discovery ([3ce271b](https://gitlab.com/opencraft/dev/grove/-/commit/3ce271b48b7807151acef583a0091b331e66ccfc))
- Minor documentation improvements. ([5ab7f4b](https://gitlab.com/opencraft/dev/grove/-/commit/5ab7f4baf0b904f129975b55883c6accb72d7807))

**Features**

- Build tools container image ([6bc0da6](https://gitlab.com/opencraft/dev/grove/-/commit/6bc0da656b330c1ea1b20f0db75a009144af1ca6))
- Register gitlab with digitalocean kubernetes ([0db6ec1](https://gitlab.com/opencraft/dev/grove/-/commit/0db6ec11e61ac88e42fd2199d71e90567d743c7e))
- Create CI job to generate deployment Merge Requests ([c7a56e9](https://gitlab.com/opencraft/dev/grove/-/commit/c7a56e955e6d99d3c92aebef59cd24f0f051fc2b))
- AWS backups ([dcab4f6](https://gitlab.com/opencraft/dev/grove/-/commit/dcab4f6cb3973680da027c06f3e5c4b3795b7d83))
- DigitalOcean backups ([f4d06ee](https://gitlab.com/opencraft/dev/grove/-/commit/f4d06ee96703e0167e0f66cff2535aa26f92c394))
- Implement DigitalOcean provider and refactor AWS one ([e56fae1](https://gitlab.com/opencraft/dev/grove/-/commit/e56fae195656051db42051466ddfd4a5c41774ef))
- Add prometheus and grafana dashboards ([dd97269](https://gitlab.com/opencraft/dev/grove/-/commit/dd972696c77d98d5eede28bdbdf171d9646b5703))
- Adds new relic monitoring for grove ([550b07f](https://gitlab.com/opencraft/dev/grove/-/commit/550b07f1002a002885618ded62b2df41ecc3706b))
- OpenSearch security configuration [SE-5432] ([f9316af](https://gitlab.com/opencraft/dev/grove/-/commit/f9316aff978c2bb557cfafb600f0e8a965524343))
- Use gitlab cluster agent instead of certificates[se-5533] ([912ad99](https://gitlab.com/opencraft/dev/grove/-/commit/912ad997e2c4599d756e5fb284b5760f9fe79966))
- Ingress and ssl for monitoring ([d99ebbe](https://gitlab.com/opencraft/dev/grove/-/commit/d99ebbefdbb8a04bf959d711e672f726a122b9b7))
- Instance archive pipeline ([c579ea2](https://gitlab.com/opencraft/dev/grove/-/commit/c579ea29fa1b4a7e0691039557010bd9bcadde43))
- Grafana dashboards for monitoring instance usage ([41d431d](https://gitlab.com/opencraft/dev/grove/-/commit/41d431d409b201c8303842288e55ad7d5de3a9c0))
- Implement periodic email notifications ([03261d6](https://gitlab.com/opencraft/dev/grove/-/commit/03261d6b3c8488ac0e4b6caacdbf5c71ac38db01))
- Implement Spaces storage for edx-platform in DO ([375248e](https://gitlab.com/opencraft/dev/grove/-/commit/375248eb3db2b2ca7496c24f09ae37b82da25529))
- Implement Spaces storage for edx-platform in DO ([f991bef](https://gitlab.com/opencraft/dev/grove/-/commit/f991befc9dac6708d64db6410fb855866cd92ac1))
- Add HPA for k8s resources ([0efdc55](https://gitlab.com/opencraft/dev/grove/-/commit/0efdc55f08cdcd340c022c6917ca128d42700fa6))
- Seetup support for static files and static pages ([e0b7ad8](https://gitlab.com/opencraft/dev/grove/-/commit/e0b7ad80a1d583b0fdeb74249308f56ef3e2173d))
- Create a VPC ([49ad00b](https://gitlab.com/opencraft/dev/grove/-/commit/49ad00b18f66952ca2be92e583905dde28952282))

**Fix**

- Terraform state wasn't saved to GitLab ([c3745c8](https://gitlab.com/opencraft/dev/grove/-/commit/c3745c8c33ab1c7709a941cb983ed3edfac2339c))
- Add in kubernetes provider configuration as required ([244d163](https://gitlab.com/opencraft/dev/grove/-/commit/244d16336e2b0866607f4dd7fc5b47edcc23f8e3))

**Miscellaneous Tasks**

- Explicitly pin click ([0c46c67](https://gitlab.com/opencraft/dev/grove/-/commit/0c46c67e63b3087a31dfc2f5c8c409678dcb17b6))
- Upgrade DO provider ([da5d020](https://gitlab.com/opencraft/dev/grove/-/commit/da5d0202706c8b01228bddd1efbf66125c084cf8))
- Bump tools-container version ([0ee912d](https://gitlab.com/opencraft/dev/grove/-/commit/0ee912dbe89ecd899d52022da22417bcae165142))
- Bump tools-container version ([c55fe66](https://gitlab.com/opencraft/dev/grove/-/commit/c55fe666b8d614167205594b0f26f48340f2891c))
- Bump default grove version ([249d4ad](https://gitlab.com/opencraft/dev/grove/-/commit/249d4ad1fd34c83b8486090a0c30cfdc25099d25))
- Updated aws s3 resources to new versions ([c12dd7d](https://gitlab.com/opencraft/dev/grove/-/commit/c12dd7d056f625ce4e966f6c0a9a09b7e7c0a6b5))
- Amazon EKS version upgraded to v18. ([343913a](https://gitlab.com/opencraft/dev/grove/-/commit/343913aba38a40df3e14d67381e49108d23a81e1))
- Prometheus now monitors all namespaces ([10f58ca](https://gitlab.com/opencraft/dev/grove/-/commit/10f58ca50830c940a4f26cac97e9aef3e7dbd299))
- Sync 'env' using object storage ([78fa0e8](https://gitlab.com/opencraft/dev/grove/-/commit/78fa0e852827108d238d22ce6bd1ef72086af157))

**Refactor**

- Use 'control' directory for all local commands ([aa887fe](https://gitlab.com/opencraft/dev/grove/-/commit/aa887febadc9daccc9852bcaee08d7fd0fe5b1ed))
- Remove the director instance ([0e8ff24](https://gitlab.com/opencraft/dev/grove/-/commit/0e8ff2460fcbe0558885097d0865f71eae846fa2))
- Remove redundancy from mysql outputs ([9e1cfc8](https://gitlab.com/opencraft/dev/grove/-/commit/9e1cfc86fd4ac3f78c6308b7c18c2aae6d5712db))
- Temporarily use a map for env buckets on DO ([e5dd66e](https://gitlab.com/opencraft/dev/grove/-/commit/e5dd66e139dbdadbc92497fb0871fccd4e5aff33))
- Replace managed redis with a helm chart ([769f263](https://gitlab.com/opencraft/dev/grove/-/commit/769f2630f3f495de6f2719fd3d3b201e2bb80c7a))
- Use-single-bucket ([acdd55c](https://gitlab.com/opencraft/dev/grove/-/commit/acdd55c69662272c58493df9bdefcebbe629eb01))
- Removed dead code and cleaned up files ([45e4d76](https://gitlab.com/opencraft/dev/grove/-/commit/45e4d76997ca5291a24847e25dbc790a30a26e07))
- Users can no longer set the monitoring namespace ([e2c0e62](https://gitlab.com/opencraft/dev/grove/-/commit/e2c0e62340b3cb7a12cfb1706afb9e99f4a1d08e))

**Build**

- Fixes multiple issues with the ci ([a9e7766](https://gitlab.com/opencraft/dev/grove/-/commit/a9e776605616336c1fff40752a515cbb4abc6a79))
- Tools-container version updated to 0.1.12 ([3cbed2f](https://gitlab.com/opencraft/dev/grove/-/commit/3cbed2fdeab1b0f896e27bdd38fbb78faddf8ef3))
- Updated tools-container version ([ff1f9c2](https://gitlab.com/opencraft/dev/grove/-/commit/ff1f9c2cb136f1b70aa4af8333d0af311205c1bf))
- Updated terraform providers and tools-container to latest ([2ea120b](https://gitlab.com/opencraft/dev/grove/-/commit/2ea120b6404dd64813bdfbdaebe74305221dcc33))
- Docs dependencies are on latest versions ([1ac8dd0](https://gitlab.com/opencraft/dev/grove/-/commit/1ac8dd0410f197eb7acc11b20b1e8fcdea8af865))
- Updated nginx ingress, cert manager, droplets, mongo  to latest ([5b78841](https://gitlab.com/opencraft/dev/grove/-/commit/5b78841ec5d89dc6c6404fb56a194d3bccb3735c))
- Larger instances required. added security rules for mysql ([81da288](https://gitlab.com/opencraft/dev/grove/-/commit/81da288c0192d95c27db0e7232a4a1a803e062ab))

**Ci**

- Fix cobertura key path ([31e17f6](https://gitlab.com/opencraft/dev/grove/-/commit/31e17f64f4890a2324ae280fb517816cfcdef0e4))

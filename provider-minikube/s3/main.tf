################################################################################
# Buckets
################################################################################
resource "aws_s3_bucket" "s3_bucket" {
  # Let us use the bucket prefix instead of bucket, so that Terraform can create
  # a identifiable unique name
  bucket_prefix = substr("${var.bucket_prefix}-", 0, 37)
  tags          = var.tags
}

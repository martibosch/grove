####################################################################################################
## Generate kubeconfig to access minikube
####################################################################################################

locals {
  default_tags = {
    ManagedBy = "Grove"
    Terraform = "true"
  }

  minikube_kubeconfig = <<EOT
apiVersion: v1
clusters:
- cluster:
    insecure-skip-tls-verify: true
    server: ${var.minikube_host}
    name: minikube
contexts:
- context:
    cluster: minikube
    namespace: default
    user: minikube
    name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
    user:
    password: no-password
    username: minikube
EOT
}

output "kubeconfig" {
  value = local.minikube_kubeconfig
}

####################################################################################################
## Integrate kubernetes cluster with the GitLab project
####################################################################################################

module "k8s_gitlab_connector" {
  source                     = "../provider-modules/k8s-gitlab-connector"
  gitlab_cluster_agent_token = var.gitlab_cluster_agent_token
}


####################################################################################################
## Create k8s secret for GitLab container registry access
####################################################################################################

module "k8s_gitlab_container_registry" {
  for_each = toset(var.tutor_instances)

  source = "../provider-modules/k8s-gitlab-container-registry"

  namespace                          = each.key
  container_registry_server          = var.container_registry_server
  dependency_proxy_server            = var.dependency_proxy_server
  gitlab_group_deploy_token_username = var.gitlab_group_deploy_token_username
  gitlab_group_deploy_token_password = var.gitlab_group_deploy_token_password
}

####################################################################################################
## Create ingress controller
####################################################################################################

module "ingress" {
  source = "../provider-modules/k8s-nginx-ingress"

  ingress_namespace = "kube-system"

  admission_webhooks_enabled = false
}
